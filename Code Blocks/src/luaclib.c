


#include "config.h"
#include "..\SDK\lua_api.h"



//您可以在lua中这样调用下面的两个函数(其中的LuaPlugin要换成自己的插件名字)
//package.cpath = "./plugin/?.so"
//plugin = require "LuaPlugin"
//print plugin.add(123,456)
//print plugin.strcat("123","456")

//您也可以在按键精灵脚本中直接调用下面的两个函数(其中的LuaPlugin要换成自己的插件名字)
//Import "LuaPlugin.so"
//TracePrint LuaPlugin.add(123,456)
//TracePrint LuaPlugin.strcat("abc","def")



static int add(lua_State *L)
{
	double param1 = lua_tonumber(L, 1);		//取得第一个参数，类型为数值型
	double param2 = lua_tonumber(L, 2);		//取得第二个参数，类型为数值型
	lua_pushnumber(L, param1 + param2);		//将两个参数之和作为返回值
	return 1;								//返回1表示有一个返回值
}



struct luaL_Reg module_name[] =
{
	{ "add", add },
	{ NULL, NULL }
};


