#pragma once

#ifdef WIN32
	
	// Exclude rarely-used stuff from Windows headers
	#define WIN32_LEAN_AND_MEAN
	
	// Windows Header Files:
	#include <windows.h>
	
	#define DLLEXPORT extern "C" __declspec(dllexport)
	
#endif

#define EXTERN_DEF extern



#define PROJECT_NAME plugin 


