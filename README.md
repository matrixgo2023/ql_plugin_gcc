﻿# BotScript Plugin Templet For MinGW

#### 项目介绍

使用这个模版，可以为 UiBot 编写符合 Lua 规范的 C/C++ 语言插件
插件可以在Lua语言中直接调用

这个模板是使用 mingw5 编写的，开发者只需要修改 src 目录下的文件即可配置插件。

推荐使用 Code::Blocks、C-Free 编辑器 (这两种开发环境都有可直接编译的工程文件) 。

编译时，会自动生成以项目名称命名的、扩展名为so的文件。将此文件复制到按键精灵的 Plugin 目录下，即可使用。
注意：扩展名为so的文件实际上就是 Windows Dll 文件，主要是为了跨平台使用和软件识别修改扩展名。

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)